# Coded By Helias (Stefano Borzì)

from profeta.main import *
from profeta.lib import *

class cinquanta(Belief):
	pass
class venti(Belief):
	pass
class dieci(Belief):
	pass
class cinque(Belief):
	pass

class restituisci(Goal):
	pass


PROFETA.start()

restituisci(0) >> [ show_line("finito") ]

restituisci("X") / (cinquanta("Y") &
				   (lambda: X >= 50) &
				   (lambda: Y > 0) &
				   (lambda: Y >= int(X / 50))) >> \
					[ "A = X/50", "TMP = Y-A", "N = X-(A*50)", -cinquanta("Y"), +cinquanta("TMP"), show_line("monete da 50: ", "A"), restituisci("N") ]

restituisci("X") / (cinquanta("Y") &
				   (lambda: X >= 50) &
				   (lambda: Y > 0) &
				   (lambda: Y < int(X / 50))) >> \
					[ "A = Y", "TMP = Y-A", "N = X-(A*50)", -cinquanta("Y"), +cinquanta("TMP"), show_line("monete da 50: ", "Y"), restituisci("N") ]

restituisci("X") / (venti("Y") &
				   (lambda: X >= 20) &
				   (lambda: Y > 0) &
				   (lambda: Y >= int(X / 20))) >> \
					[ "A = X/20", "TMP = Y-A", "N = X-(A*20)", -venti("Y"), +venti("TMP"), show_line("monete da 20: ", "A"), restituisci("N") ]

restituisci("X") / (venti("Y") &
				   (lambda: X >= 20) &
				   (lambda: Y > 0) &
				   (lambda: Y < int(X / 20))) >> \
					[ "A = Y", "TMP = Y-A", "N = X-(A*20)", -venti("Y"), +venti("TMP"), show_line("monete da 20: ", "Y"), restituisci("N") ]

restituisci("X") / (dieci("Y") &
				   (lambda: X >= 10) &
				   (lambda: Y > 0) &
				   (lambda: Y >= int(X / 10))) >> \
					[ "A = X/10", "TMP = Y-A", "N = X-(A*10)", -dieci("Y"), +dieci("TMP"), show_line("monete da 10: ", "A"), restituisci("N") ]

restituisci("X") / (dieci("Y") &
				   (lambda: X >= 10) &
				   (lambda: Y > 0) &
				   (lambda: Y < int(X / 10))) >> \
					[ "A = Y", "TMP = Y-A", "N = X-(A*10)", -dieci("Y"), +dieci("TMP"), show_line("monete da 10: ", "Y"), restituisci("N") ]

restituisci("X") / (cinque("Y") &
				   (lambda: X >= 5) &
				   (lambda: Y > 0) &
				   (lambda: Y >= int(X / 5))) >> \
					[ "A = X/5", "TMP = Y-A", "N = X-(A*5)", -cinque("Y"), +cinque("TMP"), show_line("monete da 5: ", "A"), restituisci("N") ]

restituisci("X") / (cinque("Y") &
				   (lambda: X >= 5) &
				   (lambda: Y > 0) &
				   (lambda: Y < int(X / 5))) >> \
					[ "A = Y", "TMP = Y-A", "N = X-(A*5)", -cinque("Y"), +cinque("TMP"), show_line("monete da 5: ", "Y"), restituisci("N") ]

restituisci("X") >> [ show_line("non ho monete per restituirti ", "X", "centesimi") ]

PROFETA.assert_belief(cinquanta(10))
PROFETA.assert_belief(venti(10))
PROFETA.assert_belief(dieci(10))
PROFETA.assert_belief(cinque(10))

PROFETA.run_shell(globals())
