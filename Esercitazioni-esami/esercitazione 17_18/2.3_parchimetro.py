# Coded By Helias (Stefano Borzì)

from profeta.main import *
from profeta.lib import *

class ora(Goal):
    pass
class mezzora(Goal):
    pass
class reset(Goal):
    pass

class t_ora(Belief):
    pass
class t_mezzora(Belief):
    pass

class tariffa(Goal):
    pass
class add_euro(Goal):
    pass
class add_cent(Goal):
    pass
class monete(Belief):
    pass

class check(Goal):
    pass

PROFETA.start()

# gestione dei 3 pulsanti ORA, MEZZ'ORA, RESET
ora() / t_ora("X") >> [ -t_ora("X"), "X = X+1", +t_ora("X"), show_line("ora aggiunta, ore:", "X") ]
ora() >> [ +t_ora(1), show_line("ora aggiunta, ore: 1")]

mezzora() / t_mezzora("X") >> [ -t_mezzora("X"), show_line("mezz'ora aggiunta"), ora()]
mezzora() >> [ +t_mezzora(30), show_line("mezz'ora aggunta, minuti: 30")]

reset() / t_ora("X") >> [ -t_ora("X"), -t_mezzora(30), show_line("impostazioni resettate") ]
reset() >> [ -t_mezzora(30), show_line("impostazioni resettate") ]

tariffa() / (t_ora("X") & t_mezzora("Y")) >> [ "P = (X * 50.0 + 25.0) / 100.0", show_line("tariffa: ", "P") ]
tariffa() / t_ora("X") >> [ "P = (X * 50.0) / 100.0", show_line("tariffa: ", "P") ]

add_euro("Y") / monete("X") >> [ -monete("X"), "X = X + Y*100.0", +monete("X"), "A = X/100", show_line("monete: ", "A"), check()]
add_euro("Y") >> [ "Y = Y*100", "A = Y/100.0", +monete("Y"), show_line("monete: ", "A"), check() ]

add_cent("Y") / monete("X") >> [ -monete("X"), "X = X + Y", +monete("X"), "A = X / 100.0", show_line("monete: ", "A"), check()]
add_cent("Y") >> [ "A = Y", +monete("Y"), show_line("monete: ", "A"), check() ]

check() / (monete("K") & t_ora("X") & t_mezzora("Y") & (lambda : (X * 50.0 + 25.0) / 100.0 <= K/100.0) ) >> [ show_line("tariffa raggiunta, emissione del ticket....") ]
check() / (monete("K") & t_ora("X") & (lambda : (X * 50.0) / 100.0 <= K/100.0)  ) >> [ show_line("tariffa raggiunta, emissione del ticket....") ]

PROFETA.run_shell(globals())
