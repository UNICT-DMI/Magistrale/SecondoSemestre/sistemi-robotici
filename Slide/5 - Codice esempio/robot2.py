#
# robot2.py
#
import pylab

class Robot:

	def __init__(self, massa, attrito):
		self.__massa = massa
		self.__attrito = attrito
		self.__vel = 0
		self.__pos = 0

	def evaluate(self, f, delta_t):
		self.__pos = self.__pos + self.__vel * delta_t
		self.__vel = (-self.__attrito * delta_t / self.__massa + 1) * self.__vel + \
				delta_t / self.__massa * f

	def get_vel(self):
		return self.__vel

	def get_pos(self):
		return self.__pos


robottino = Robot(5, 0.6)

delta_t = 0.01 # 10ms di tempo di campionamento
durata_simulazione = 20 # secondi

numero_punti = int(durata_simulazione / delta_t)

tempi = []
speed = []
position = []
f = 10 # newton
for k in range(0, numero_punti):
	t = k*delta_t
	robottino.evaluate(f, delta_t)
	tempi.append(t)
	speed.append(robottino.get_vel())
	position.append(robottino.get_pos())

pylab.plot(tempi, speed)
pylab.plot(tempi, position)
pylab.show()

