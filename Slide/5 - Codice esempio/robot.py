#
# robot.py
#
import pylab

b = 0.3
M = 5
delta_t = 0.01 # 10ms di tempo di campionamento
durata_simulazione = 240 # secondi

numero_punti = int(durata_simulazione / delta_t)

tempi = []
speed = []
v = 0
f = 10 # newton
for k in range(0, numero_punti):
	t = k*delta_t
	tempi.append(t)
	v = (-b*delta_t/M + 1) * v + delta_t / M * f
	speed.append(v)
	f = 0

pylab.plot(tempi, speed)
pylab.show()

